package pl.akademiakodu.Lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LottoController {
    @GetMapping("/")
    public String getSixNumbers(ModelMap modelMap){
        modelMap.put("lottoNumbers", GeneratorLotto.getTotek());
        return "HomeLotto";
    }

}
