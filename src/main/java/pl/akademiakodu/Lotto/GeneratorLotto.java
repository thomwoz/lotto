package pl.akademiakodu.Lotto;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class GeneratorLotto {
    public static Set<Integer> getTotek(){
        Set<Integer> totek = new TreeSet<>();
        Random generator = new Random();

        while(totek.size()<6) {
            totek.add(generator.nextInt(49)+1);
        }
        return totek;
    }
}
